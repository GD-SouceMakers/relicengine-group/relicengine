##The  goal of the game:  

Is to make a game where you have to advance in a labyrinth, while avoiding and fighting basic enemies.
To do so you will have to find keys to doors that will lead you further into the dungeon.

###The controls
You can move your character with the arrow keys or ``wasd``, use the ``space`` to hit, and with ``e`` use the objects around you.

##The Engine 

The goal is to have an engine that facilitates the needs of this game and nothing more for the time.

###The files  
The goal is to read in maps that are in the ``.rmf`` format and the tile sets from the ``.rsf`` files.
The game also reads in a ``.rpf`` file that contains information about the player.

The final product will probably use more file types but those can only be fully developed latter in development


 