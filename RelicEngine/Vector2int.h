#ifndef VECTOR_2_H
#define VECTOR_2_H

#include "math.h"

double to_degrees(double radians);
double clamp(double d, double min, double max);


enum {
    NORTH = 0,
    SOUTH = 1,
    WEST = 2,
    EAST = 3
};

int degrees_to_NSWE(double angle);

struct Vector2int;

typedef struct Vector2int {
    int x;
    int y;
} Vector2int_t;

typedef struct Vector2double {
    double x;
    double y;
} Vector2double_t;

void Vector2int_Add(Vector2int_t *a, Vector2int_t b);

void Vector2int_Substract(Vector2int_t *a, Vector2int_t b);

void Vector2int_Multply(Vector2int_t *a, Vector2int_t b);

void Vector2int_Multplyint(Vector2int_t *a, int b);

Vector2double_t Vector2int_to_double(Vector2int_t a);


void Vector2double_Add(Vector2double_t *a, Vector2double_t b);

void Vector2double_Substract(Vector2double_t *a, Vector2double_t b);
void Vector2double_Multply(Vector2double_t *a, Vector2double_t b);
void Vector2double_Multplyint(Vector2double_t *a, int b);
void Vector2double_Multplydouble(Vector2double_t *a, double b);

double Vector2double_Magnitude(Vector2double_t a);

void Vector2double_Normalize(Vector2double_t *a);

Vector2double_t Vector2double_LeftNormal(Vector2double_t a);
Vector2double_t Vector2double_RightNormal(Vector2double_t a);

double Vector2double_DotProduct(Vector2double_t a, Vector2double_t b);

Vector2double_t Vector2double_Lerp(Vector2double_t a, Vector2double_t b, int i);
double Vector2double_Distance(Vector2double_t a, Vector2double_t b);
double Vector2double_Angle(Vector2double_t a);


Vector2int_t Vector2double_to_int(Vector2double_t a);

#endif // !VECTOR_2_H

