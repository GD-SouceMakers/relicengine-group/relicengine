#include "RelicApp.h"
#include "RelicTextureManager.h"

#include "RelicMap/RelicMap.h"

#include "RelicTimer.h"
#include "Input.h"
#include "RelicEngine/RelicEntitys/RelicPlayer.h"
#include "RelicEntitys/RelicEntity.h"
#include "RelicGUI.h"

RelicApp *g_relicapp;

//FUNCTIONS
void sdl_init();

void createWindow(int width, int height, SDL_Window **pwindow);

void createDebugImage(int width, int height, RelicRenderer *renderer);

void HandleInput(RelicApp *self, SDL_Event event);

//To init all the subsystems of the app
void InitApp(RelicApp *self) {
    g_relicapp = self;


    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);

    timeSinceStart = 0;
    deltaTime = 0;

    sdl_init();


    RelicCreateRenderer();


    g_mapManager = Create_MapManager();
    g_textureManager = Create_TextureManager();

    SetupGUI();


    RegisterDefaults();
}

//To start the main update loop
void StartApp(RelicApp *self) {
    //Here in the final game we would have the main gui

    //LoadMap(g_mapManager, "Map-1-0",NULL);


    SDL_Event event;
    int running = 1;
    while (running) {
        timeSinceStart = SDL_GetTicks();

        //SDL_Log("Running");
        //Get the events that have qued up since the last run
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = 0;
                    break;
                default:
                    HandleInput(self, event);
                    break;
            }
        }

        if(self->state == END_GAME){
            EndGame();
        }
        else if (self->state == NEW_MAP) {
            DoMapLoad(g_mapManager);

            StartAllEntities(g_mapManager->activeMap);
            SetDefaultLightLevel(g_mapManager->activeMap->defaultLight);
            SetOverlayLightLevel(0);
            self->state = IN_GAME;
        }
        if (g_mapManager->activeMap != NULL) {
            UpdateAllEntities(g_mapManager->activeMap);

            PhysicsUpdateAllEntities(g_mapManager->activeMap);

            LateUpdateAllEntities(g_mapManager->activeMap);

            //Draw the screen (should be in a separate thread.... but this is C so... sad)

            DrawMap(g_mapManager->activeMap);
        }
        DrawGUI();

        Present(g_renderer);

        Clear(g_renderer);

        deltaTime = ((SDL_GetTicks()/1000.f) - timeSinceStart/1000.f);
        if (deltaTime > 10) {
            deltaTime = 0.5;

        }

        timeSinceStart = SDL_GetTicks();

        //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%lld",deltaTime);
    }


    FreeTextureManager(g_textureManager);
    FreeRegistry();
    FreeRenderer();
    FreeMapManager();
}


//Init the sdl subsystem
void sdl_init() {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());

        exit(-1);
    }

    if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) == 0) {
        SDL_Log("Nem indithato az SDL_image: %s", SDL_GetError());

        exit(-1);
    }


}


//Process the SDL events 
void HandleInput(RelicApp *self, SDL_Event event) {

    switch (event.type) {
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
                case SDLK_LEFT:
                    globalInput.left = -1;
                    break;
                case SDLK_RIGHT:
                    globalInput.left = 1;
                    break;
                case SDLK_UP:
                    globalInput.forward = -1;
                    break;
                case SDLK_DOWN:
                    globalInput.forward = 1;
                    break;
                case SDLK_RETURN:
                    globalInput.mouse_hold = true;
                    break;
            }
            break;
        case SDL_KEYUP:
            switch (event.key.keysym.sym) {
                case SDLK_LEFT:
                    globalInput.left = 0;
                    break;
                case SDLK_RIGHT:
                    globalInput.left = 0;
                    break;
                case SDLK_UP:
                    globalInput.forward = 0;
                    break;
                case SDLK_DOWN:
                    globalInput.forward = 0;
                    break;
                case SDLK_RETURN:
                    globalInput.mouse_hold = false;
                    break;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            globalInput.mouse_hold = false;
            break;
        case SDL_MOUSEBUTTONDOWN:
            globalInput.mouse_hold = true;
            break;
        case SDL_MOUSEMOTION:
            SDL_GetMouseState(&globalInput.mouse_x, &globalInput.mouse_y);
            break;
    }

}

void ResetInput() {
    globalInput.esc = 0;
    globalInput.forward = 0;
    globalInput.left = 0;
}


void StartGame(){
    LoadMap(g_mapManager, "Map-1-1",NULL);
    SDL_LogError(RELICLOG_CATEGORY_DEBUG,"StartNewGame");
}

void EndGame(){
    UnloadMap(g_mapManager);
}