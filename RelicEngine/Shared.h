#ifndef SHARED_H
#define SHARED_H

#include "stdbool.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <SDL2/SDL.h>
#include "SDL2/SDL_render.h"
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_main.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>

#include "Vector2int.h"

#include "lib/debugmalloc.h"

enum {
    RELICLOG_CATEGORY_FILE_LOAD = SDL_LOG_CATEGORY_CUSTOM,
    RELICLOG_CATEGORY_MEMORY_ALLOC,
    RELICLOG_CATEGORY_MAP,
    RELICLOG_CATEGORY_MAP_DRAW,
    RELICLOG_CATEGORY_DEBUG = SDL_LOG_PRIORITY_DEBUG,
    RELICLOG_CATEGORY_ERROR = SDL_LOG_PRIORITY_ERROR
};

#endif // !SHARED_H

