#ifndef	RELIC_MAP_LOAD_H
#define RELIC_MAP_LOAD_H

#define _CRT_SECURE_NO_WARNINGS

struct RelicMap_t;
struct RelicPlayer;

#include "RelicMap.h"
#include "RelicEntitys/RelicRegisty.h"

RelicMap_t* ParseMapFile(char *filename);
struct RelicTexture * ParseSheetFile(char *name);

typedef struct Action {

    char name[32];

    int strite;

} Action;

typedef struct CharacterData{
    char*name[32];
    struct RelicTexture *characterSheat;

    Action *actions;
    int actionCount;

    ValueKey *propertyes;
}CharacterData;

CharacterData* ParseCharacterFile(char *name);

void FreeValuKeyList( ValueKey *first);

#endif // !RELIC_MAP_LOAD_H

