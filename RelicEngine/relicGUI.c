//
// Created by dpete on 11/20/2018.
//

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>
#include <RelicEngine/RelicEntitys/RelicPlayer.h>
#include "RelicGUI.h"
#include "RelicApp.h"
#include "RelicTextureManager.h"
#include "Input.h"
#include "Vector2int.h"

typedef enum GUIStates {
    GUI_INTRO,
    GUI_MAIN_MENU,
    GUI_IN_GAME,
    GUI_END_GAME
} GUIStates;

GUIStates guiStates;

SDL_Texture *base;
SDL_Texture *top;

TTF_Font *HpFont;
TTF_Font *IntroFont;
TTF_Font *IntroOutlineFont;
TTF_Font *MenuFont;
TTF_Font *MenuOutlineFont;

SDL_Color Gr_outlineColor = (SDL_Color) {50, 1, 1, 255};
SDL_Color Gr_textColor = (SDL_Color) {155, 0, 0, 255};

RelicTexture_t *IngameGUI;
RelicTexture_t *MenuGUI;

SDL_Texture *PreProcess;

double timer;

int windowHeight;
int windowWidth;

typedef void(*GUIScreenCall)();
enum GUIScreenState {
    GUISCREEN_SETUP,
    GUISCREEN_FADE_IN,
    GUISCREEN,
    GUISCREEN_FADE_OUT
};
void GUIScreenDraw(GUIScreenCall call, enum GUIScreenState *state, GUIStates nextScreen, GUIScreenCall exit);

void DrawgrimphobiaText(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, SDL_Color text_color,
                        SDL_Color outline_color);

typedef void(*DrawEvent)(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width,
                         int height);

bool IMGUIButtonCentered(Vector2int_t pos, char *text, TTF_Font *font, TTF_Font *outline,
                         DrawEvent normal, DrawEvent
                         hover,
                         DrawEvent presssed
);

void DrawButtonNormal(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height);

void DrawButtonHover(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height);

void DrawButtonPressed(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height);

void Intro();


const double introLenght = 2;
const double introFade = 0.25;
bool outfade;

void SetupGUI() {
    IntroFont = TTF_OpenFont("UnZialish.ttf", 70);
    IntroOutlineFont = TTF_OpenFont("UnZialish.ttf", 70);
    TTF_SetFontOutline(IntroOutlineFont, 4);

    MenuFont = TTF_OpenFont("UnZialish.ttf", 30);
    MenuOutlineFont = TTF_OpenFont("UnZialish.ttf", 30);
    TTF_SetFontOutline(MenuOutlineFont, 4);


    HpFont = TTF_OpenFont("UnZialish.ttf", 20);

    IngameGUI = GetTexture(g_textureManager, "gui_ingame_01");
    MenuGUI = GetTexture(g_textureManager, "gui_menu_01");

    PreProcess = SDL_CreateTexture(g_renderer->sdlRenderer, SDL_PIXELFORMAT_RGBA8888,
                                   SDL_TEXTUREACCESS_TARGET, g_renderer->width, g_renderer->height);
    SDL_SetTextureBlendMode(PreProcess, SDL_BLENDMODE_BLEND);


    windowHeight = g_renderer->height;
    windowWidth = g_renderer->width;

    guiStates = GUI_INTRO;
};

//Intro Draw
void Intro() {

    DrawgrimphobiaText("Grimphobia", IntroFont, IntroOutlineFont,
                       (struct Vector2int) {windowWidth / 2, windowHeight / 2},
                       Gr_textColor,
                       Gr_outlineColor);

    if (timer >= introLenght) {
        int a = clamp((timer - introLenght) / introFade * 255, 0, 255);
        //SDL_LogError(RELICLOG_CATEGORY_DEBUG, "%d", a);
        SetOverlayLightLevel(a);
        if (timer - introLenght > introFade) {
            guiStates = GUI_MAIN_MENU;
        }
    }
}

//Ingame
enum GUIScreenState inGameState = GUISCREEN_SETUP;
enum GUIScreenState inGameState_last = GUISCREEN_SETUP;
void DrawInGameCotent(){
    int hp_offset = 110 * ((double) (g_player->max_hp - g_player->hp) / g_player->max_hp);

    RenderGUISprite(NULL, &IngameGUI->sprites[0], NULL, (Vector2int_t) {0, 0}, (SDL_Color) {255, 255, 255, 255}, true);

    RenderGUISprite(PreProcess, &IngameGUI->sprites[2], NULL, (Vector2int_t) {0, 0}, (SDL_Color) {255, 0, 0, 255},
                    true);
    RenderGUIRect(PreProcess, (Vector2int_t) {0, 25}, 120, hp_offset, (SDL_Color) {0, 0, 0, 0});

    RenderTextureToTarget(PreProcess, g_renderer->guiTarget);
    ClearTexture(PreProcess, (SDL_Color) {0, 0, 0, 0});


    RenderGUISprite(NULL, &IngameGUI->sprites[1], NULL, (Vector2int_t) {0, 0}, (SDL_Color) {255, 255, 255, 255}, true);

    RenderGUIText(NULL, (struct Vector2int) {0, 0}, HpFont, "asd", (SDL_Color) {255, 150, 150, 255});

    if(!g_player->alive && inGameState != GUISCREEN_FADE_OUT) {
        if(inGameState == GUISCREEN) {
            SDL_LogError(RELICLOG_CATEGORY_DEBUG,"in game GUISCREEN_FADE_OUT");
            inGameState = GUISCREEN_FADE_OUT;
            timer = 0;
        }
    }
}
void InGameExit(){
    g_relicapp->state = END_GAME;
}


//EndGame
enum GUIScreenState endState = GUISCREEN_SETUP;
enum GUIScreenState endState_last = GUISCREEN_SETUP;
void DrawEndMenuCotent(){
    RenderGUIRect(NULL,(Vector2int_t){0,0},windowWidth,windowHeight,(SDL_Color){0,0,0,255});

    DrawgrimphobiaText("END (WIP)", IntroFont, IntroOutlineFont,
                       (struct Vector2int) {windowWidth / 2, windowHeight / 2 - 150},
                       Gr_textColor,
                       Gr_outlineColor);

    if (IMGUIButtonCentered((struct Vector2int) {windowWidth / 2, windowHeight / 2 - 0}, "Back", MenuFont,
                            MenuOutlineFont,
                            DrawButtonNormal, DrawButtonHover, DrawButtonPressed)) {
        if(endState == GUISCREEN) {
            SDL_LogError(RELICLOG_CATEGORY_DEBUG,"end GUISCREEN_FADE_OUT");
            endState = GUISCREEN_FADE_OUT;
            timer = 0;

        }
    }
}
void EndMenuExit(){
    EndGame();
}


//MainMenu draw
enum GUIScreenState menuState = GUISCREEN_SETUP;
enum GUIScreenState menuState_last = GUISCREEN_SETUP;
void DrawMainMenuCotent() {
    DrawgrimphobiaText("Grimphobia", IntroFont, IntroOutlineFont,
                       (struct Vector2int) {windowWidth / 2, windowHeight / 2 - 150},
                       Gr_textColor,
                       Gr_outlineColor);

    //RenderGuiTextCentered(NULL, (struct Vector2int) {windowWidth/2, windowHeight/2-100}, MenuFont, "Play",(SDL_Color){155,0,0,255});
    if (IMGUIButtonCentered((struct Vector2int) {windowWidth / 2, windowHeight / 2 - 0}, "PLAY", MenuFont,
                            MenuOutlineFont,
                            DrawButtonNormal, DrawButtonHover, DrawButtonPressed)) {
        if(menuState == GUISCREEN) {
            SDL_LogError(RELICLOG_CATEGORY_DEBUG,"main menu GUISCREEN_FADE_OUT");
            menuState = GUISCREEN_FADE_OUT;
            timer = 0;
        }
    }
}
void MainMenuExit(){
    StartGame();
}

void DrawGUI() {
    //Draw the base
    //content
    //top

    switch (guiStates) {
        case GUI_INTRO:
            Intro();
            break;
        case GUI_MAIN_MENU:
            //MainMenu();
            GUIScreenDraw(DrawMainMenuCotent,&menuState,GUI_IN_GAME,MainMenuExit);
            break;
        case GUI_IN_GAME:
            //InGameGUI();
            GUIScreenDraw(DrawInGameCotent,&inGameState,GUI_END_GAME, InGameExit);
            break;
        case GUI_END_GAME:
            //EndGameGui();
            GUIScreenDraw(DrawEndMenuCotent,&endState,GUI_MAIN_MENU,EndMenuExit);
            break;
    }
    timer += deltaTime;
/*
    if(menuState != menuState_last || inGameState != inGameState_last || endState != endState_last) {
        SDL_LogError(RELICLOG_CATEGORY_DEBUG, "[GUI]: menu: %d, in_game: %d, end: %d", menuState, inGameState, endState);
        menuState_last = menuState;
        inGameState_last = inGameState;
        endState_last = endState;
    }
    */
}



void GUIScreenDraw(GUIScreenCall call, enum GUIScreenState *state, GUIStates nextScreen, GUIScreenCall exit){
    int a;
    switch (*state) {
        case GUISCREEN_SETUP:
            timer = 0;
            *state = GUISCREEN_FADE_IN;
        case GUISCREEN_FADE_IN:
            call();

            a = clamp(((introFade - timer) / introFade) * 255, 0, 255);
            //SDL_LogError(RELICLOG_CATEGORY_DEBUG, "%d", a);
            SetOverlayLightLevel(a);

            if (timer >= introFade) {
                *state = GUISCREEN;
                SetOverlayLightLevel(0);
                timer = 0;
            }

            break;

        case GUISCREEN:
            call();
            break;
        case GUISCREEN_FADE_OUT:
            call();

            a = clamp((timer) / introFade * 255, 0, 255);
            //SDL_LogError(RELICLOG_CATEGORY_DEBUG, "%d", a);
            SetOverlayLightLevel(a);

            if (timer >= introFade) {
                exit();
                guiStates = nextScreen;
                *state = GUISCREEN_SETUP;
                timer = 0;
            }
            break;
    }
}




void DrawgrimphobiaText(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, SDL_Color text_color,
                        SDL_Color outline_color) {
    RenderGuiTextCentered(NULL, pos, outline, text, outline_color);
    RenderGuiTextCentered(NULL, pos, font, text, text_color);
}

bool
IMGUIButtonCentered(Vector2int_t pos, char *text, TTF_Font *font, TTF_Font *outline, DrawEvent normal, DrawEvent hover,
                    DrawEvent presssed) {
    int xmPos = globalInput.mouse_x, ymPos = globalInput.mouse_y;
    int mState = globalInput.mouse_hold;

    SDL_Rect size = GetRenderGUITextSize(outline, text);
    size.x = pos.x - size.w / 2;
    size.y = pos.y - size.h / 2;
    //If the mouse moved
    if (!mState) {
        //If the mouse is over the button
        if ((xmPos > size.x) && (xmPos < size.x + size.w) && (ymPos > size.y) && (ymPos < size.y + size.h)) {
            hover(text, font, outline, pos, size.w, size.h);
        } else {
            normal(text, font, outline, pos, size.w, size.h);
        }
    } else {
        if ((xmPos > size.x) && (xmPos < size.x + size.w) && (ymPos > size.y) && (ymPos < size.y + size.h)) {
            presssed(text, font, outline, pos, size.w, size.h);
            return true;
        } else {
            normal(text, font, outline, pos, size.w, size.h);
        }
    }
    return false;
}

void DrawButtonNormal(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height) {
    //RenderGuiTextCentered(NULL, pos, font, text, (SDL_Color) {155, 0, 0, 255});
    DrawgrimphobiaText(text, font, outline, pos, Gr_textColor,
                       Gr_outlineColor);
}

void DrawButtonHover(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height) {
    //RenderGuiTextCentered(NULL, pos, font, "Play", (SDL_Color) {155, 0, 0, 255});

    DrawgrimphobiaText(text, font, outline, pos, Gr_textColor,
                       Gr_outlineColor);


    pos.x += width / 2;
    pos.y -= MenuGUI->sprites[0].height / 2;
    RenderGUISprite(NULL, &MenuGUI->sprites[0], NULL, pos, (SDL_Color) {255, 255, 255, 255}, false);
}

void DrawButtonPressed(char *text, TTF_Font *font, TTF_Font *outline, Vector2int_t pos, int width, int height) {
    //RenderGuiTextCentered(NULL, pos, font, "Play", (SDL_Color) {155, 0, 0, 255});
    DrawgrimphobiaText(text, font, outline, pos, Gr_textColor,
                       Gr_outlineColor);
}