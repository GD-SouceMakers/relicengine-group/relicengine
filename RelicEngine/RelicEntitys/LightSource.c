//
// Created by dpete on 11/16/2018.
//

#include <RelicEngine/RelicTextureManager.h>
#include "LightSource.h"
#include "RelicEntity.h"
#include "RelicRegisty.h"

typedef struct LightData{
    char* image;
    RelicTexture_t *lightTexture;
    int sprite;
    SDL_Color color;
} LightData;


void DrawLigthSource(RelicEntity_t *self){
    LightData *data = ((LightData*)self->data);

    Sprite_t *sprite = &data->lightTexture->sprites[data->sprite];
    Vector2double_t shadowPos = self->pos;
    Vector2double_Substract(&shadowPos, (Vector2double_t) {.x=sprite->width / 2, .y=sprite->height / 2});
    RenderSprite_lighting_double(g_renderer,sprite,NULL,shadowPos,data->color,false);
}

void FreeLightSource(RelicEntity_t *self){
    LightData *data = ((LightData*)self->data);
    free(data->image);
}

RelicEntity_t* CreateLightSource(Vector2double_t pos,char*type,char*name, ValueKey *data){
    struct RelicEntity *res =malloc(sizeof(struct RelicEntity));
    SetupEntity(res,type,name,pos,false);

    SetupFunctions(res,NULL,NULL,NULL,NULL,DrawLigthSource,FreeLightSource);

    res->data = malloc(sizeof(LightData));
    LightData *lightdata = ((LightData*)res->data);

    lightdata->color = GetColorValueKey(data,"color");
    lightdata->sprite = GetIntValueKey(data,"sprite");
    lightdata->image = GetStringValueKey(data,"image");

    lightdata->lightTexture = GetTexture(g_textureManager,lightdata->image);


    return res;
}

void RegisterLightSource(){
    RegisterEntity("light_source",&CreateLightSource);
}