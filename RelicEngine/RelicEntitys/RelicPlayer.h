#ifndef RELIC_PLAYER_H_
#define RELIC_PLAYER_H_

#include "Shared.h"

#include "RelicRenderer.h"
#include "RelicFileLoad.h"
#include "RelicPhysics.h"


enum {
    RELICPLAYER_MOVEMENT_UP = 1,
    RELICPLAYER_MOVEMENT_DOWN,
    RELICPLAYER_MOVEMENT_LEFT,
    RELICPLAYER_MOVEMENT_RIGHT,
	RELICPLAYER_MOVEMENT_DIE
};

typedef struct RelicPlayer
{
	char*name[32];

	int hp;
	int max_hp;

	bool alive;


	double speed;



	int movement;

	//struct RelicTexture *player_sheet;
	//char (*actions)[20];
	//int *actionSprites;
	//int actionCount;

	CharacterData *characterData;

	struct RelicTexture *light_sheet;

	Vector2double_t offset;

} RelicPlayer_t;

void RegisterPlayer();

void SetupThePlayer(RelicPlayer_t *player);

extern RelicPlayer_t *g_player;

#endif // !RELIC_PLAYER_H_
