//
// Created by dpete on 11/5/2018.
//
#include <RelicEngine/RelicEntitys/RelicPlayer.h>
#include "RelicCamera.h"

#include "RelicEngine/RelicEntitys/RelicEntity.h"
#include "RelicRegisty.h"


const char typeName[] = "camera";

RelicEntity_t *mainCamera;

typedef struct CameraData {
    int height;
    int width;
    double eventHorizont;
    RelicEntity_t *player;
} CameraData;

void WorldToScreen(RelicEntity_t *self, Vector2int_t *a) {
    const Vector2int_t center = (Vector2int_t) {.x = 500 / 2, .y=500 / 2};

    Vector2int_Substract(a, Vector2double_to_int(self->pos));
    Vector2int_Add(a, center);
}

double OutRangeClamp(double value, double Min, double Max) {
    if (value < Min) {
        return value - Min;
    } else if (value > Max) {
        return value - Max;
    }
    return 0;
}

void UpdateCamera(RelicEntity_t *self) {
    //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"Camera Update");
    double xCamPos = self->pos.x;
    double yCamPos = self->pos.y;

    double xPos = ((CameraData*)self->data)->player->pos.x;
    double yPos = ((CameraData*)self->data)->player->pos.y;

    double xCameraH = ((CameraData *) self->data)->width / 2.0;
    double yCameraH = ((CameraData *) self->data)->height / 2.0;

    self->pos = (Vector2double_t) {
            clamp(xPos,
                  xCameraH,
                  g_mapManager->activeMap->width * g_mapManager->activeMap->tileWidth - xCameraH),
            clamp(yPos+1,
                  yCameraH,
                  g_mapManager->activeMap->height * g_mapManager->activeMap->tileHeight - yCameraH)
    };

    /*self->pos = (Vector2double_t) {
            clamp(
                    xCamPos + OutRangeClamp(xPos,
                                            xCamPos - xCameraH / ((CameraData *) self->data)->eventHorizont,
                                            xCamPos + xCameraH / ((CameraData *) self->data)->eventHorizont),
                    xCameraH,
                    g_mapManager->activeMap->height * g_mapManager->activeMap->tileHeight - xCameraH
            ),

             clamp(
                    yCamPos + OutRangeClamp(yPos,
                                            yCamPos - yCameraH / ((CameraData *) self->data)->eventHorizont,
                                            yCamPos + yCameraH / ((CameraData *) self->data)->eventHorizont),
                    yCameraH,
                    g_mapManager->activeMap->width * g_mapManager->activeMap->tileWidth - yCameraH
            )};
            */
    //self->pos = g_player.pos;
}

void StartCamera(RelicEntity_t *self) {
    SDL_Log("Camera Start");

    RelicEntity_t *a = GetEntityByType(g_mapManager->activeMap, "player");
if(a != NULL)
    ((CameraData *) self->data)->player = a;
else
    SDL_LogError(RELICLOG_CATEGORY_DEBUG,"NO player");

}

RelicEntity_t *CreateCamera(Vector2double_t pos, char *type, char *name, ValueKey *data) {
    struct RelicEntity *res = malloc(sizeof(struct RelicEntity));

    SetupEntity(res, type, name, pos, true);

    SetupFunctions(res, &StartCamera, NULL, &UpdateCamera,NULL, NULL,NULL);


    res->data = malloc(sizeof(CameraData));
    ((CameraData *) res->data)->width = 500;
    ((CameraData *) res->data)->height = 500;
    ((CameraData *) res->data)->eventHorizont = 100;

    mainCamera = res;

    return res;

}

void RegisterCamera() {
    RegisterEntity(typeName, &CreateCamera);
}