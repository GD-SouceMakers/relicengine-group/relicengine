#include <Program.h>
#include "MapChange.h"

#include "RelicMap.h"
#include "RelicRegisty.h"


enum State{
    FADING_IN,
    FADING_OUT,
    STANDBY
};

typedef struct MapChangeData {

    enum State state;


    char destMapName[32];
    char destObjectName[32];


    int fadeDir;
    double fadeSpeed;
    double fadePregress;
} MapChangeData;


void UpdateMapChange(RelicEntity_t *self) {

    MapChangeData *data = (MapChangeData *) self->data;

    struct RelicEntity *collision = self->physics.collision;
    switch (data->state){
        case STANDBY:
            if (collision != NULL && strcmp(collision->type, "player") == 0) {
                data->state = FADING_IN;
                SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Collided with player");
                g_relicapp->state = MAP_TRANSITION;
            }
            break;
        case FADING_IN:
            data->fadePregress += data->fadeSpeed * deltaTime;
            if (data->fadePregress >= 1) {
                data->state = FADING_OUT;
                self->doNotDestroyOnLoad = true;
                LoadMap(g_mapManager, data->destMapName, data->destObjectName);

            }
            else{
                Uint8 light = floor(data->fadePregress*255);
                SetOverlayLightLevel(light);
                //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%d", light);
            }
            break;
        case FADING_OUT:
            data->fadePregress -= data->fadeSpeed * deltaTime;
            if (data->fadePregress <= 0) {
                data->state = STANDBY;
                g_relicapp->state = IN_GAME;
                SetOverlayLightLevel(0);
                DestroyEntity(g_mapManager->activeMap,self);
            }
            else{
                Uint8 light = floor(data->fadePregress*255);
                SetOverlayLightLevel(light);
                //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%d", light);
                g_relicapp->state = MAP_TRANSITION;
            }
            break;
    }

}

void LateUpdateMapChange(RelicEntity_t *self) {

}

void DrawMapChange(RelicEntity_t *self) {

}

void StartMapChange(RelicEntity_t *self) {
    SDL_Log("Empty Start");
}

RelicEntity_t *CreateMapChange(Vector2double_t pos, char *type, char *name, ValueKey *data) {

    double width, height;
    char destmap[32], destObj[32];

    ValueKey *next = data;
    while (next != NULL) {

        if (strcmp(next->key, "width") == 0) {
            width = strtod(next->value, NULL);
        } else if (strcmp(next->key, "height") == 0) {
            height = strtod(next->value, NULL);
        } else if (strcmp(next->key, "dest_map") == 0) {
            strcpy(destmap, next->value);
        } else if (strcmp(next->key, "dest_obj") == 0) {
            strcpy(destObj, next->value);
        }

        next = next->next;
    }

    struct RelicEntity *res = malloc(sizeof(struct RelicEntity));
    SetupEntity(res, type, name, pos, false);


    Shape *s = CreateRect(width, height);
    TranslateShape(s, (Vector2double_t){width/2,height/2});
    SetupPhysics(res, true, s);


    SetupFunctions(res, &StartMapChange, &UpdateMapChange, &LateUpdateMapChange,NULL, &DrawMapChange, NULL);

    res->data = malloc(sizeof(MapChangeData));
    MapChangeData *a = ((MapChangeData *) res->data);
    strcpy(a->destMapName, destmap);
    strcpy(a->destObjectName, destObj);
    a->fadePregress = 0;
    a->fadeDir = 0;
    a->fadeSpeed = 2;
    a->state = STANDBY;

    return res;
}

void RegisterMapChange() {
    RegisterEntity("map_change", &CreateMapChange);
}