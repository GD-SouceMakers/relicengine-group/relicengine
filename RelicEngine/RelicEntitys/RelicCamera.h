//
// Created by dpete on 11/5/2018.
//

#ifndef RELICENGINE_RELICCAMERA_H
#define RELICENGINE_RELICCAMERA_H


#include <RelicEngine/Vector2int.h>
#include "RelicEntity.h"

void RegisterCamera();
void WorldToScreen(RelicEntity_t *self, Vector2int_t *a);

extern RelicEntity_t *mainCamera;

#endif //RELICENGINE_RELICCAMERA_H
