//
// Created by dpete on 11/18/2018.
//

#include <RelicEngine/RelicTextureManager.h>
#include "Spider.h"
#include "RelicEntity.h"
#include "RelicRegisty.h"
#include "RelicMap.h"
#include "RelicFileLoad.h"

typedef enum SpiderStates {

    MOVEMENT_UP,
    MOVEMENT_DOWN,
    MOVEMENT_LEFT,
    MOVEMENT_RIGHT,
    ATTACK_UP,
    ATTACK_DOWN,
    ATTACK_LEFT,
    ATTACK_RIGHT,
    IDLE,
    MOVEMENT_DIE

} SpiderStates;

typedef struct SpiderData {
    double speed;

    double cooldown;
    double cooldown_current;

    char (*corners)[32];
    int cornerCount;
    int dstCorner;

    SpiderStates state;

    CharacterData *characterData;
} SpiderData;

Vector2double_t GetHeadingDir(RelicEntity_t *self, Vector2double_t *dst);

int GetActionSpider(SpiderData *self, char *name) {
    for (int i = 0; i < self->characterData->actionCount; ++i) {
        if (strcmp(self->characterData->actions[i].name, name) == 0) {
            return i;
        }
    }
    SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Can't find the action: %s", name);
    return -1;
}


void StartSpider(RelicEntity_t *self) {

}

void UpdateSpider(RelicEntity_t *self) {
    SpiderData *data = ((SpiderData *) self->data);
    //Use a lerp function to move the spider

    if(self->physics.collision != NULL && strcmp( self->physics.collision->type,"player") == 0){
        if(data->cooldown_current >= data->cooldown) {
            self->physics.collision->Hurt(self->physics.collision, 1);
            data->cooldown_current = 0;
        }
        data->cooldown_current += deltaTime*1000;

        Vector2double_t heading = self->physics.collision->pos;
        Vector2double_Substract(&heading, self->pos);
        Vector2double_Normalize(&heading);

        double angle = 90 + to_degrees(Vector2double_Angle(heading));
        int foward = degrees_to_NSWE(angle);
        switch (foward) {
            case NORTH:
                data->state = ATTACK_UP;
                break;
            case EAST:
                data->state = ATTACK_RIGHT;
                break;
            case SOUTH:
                data->state = ATTACK_DOWN;
                break;
            case WEST:
                data->state = ATTACK_LEFT;
                break;
            default:
                break;
        }
        self->physics.velocity = (Vector2double_t){0,0};

    }
    else {
        data->cooldown_current = 0;

        Vector2double_t dst = GetEntityByName(g_mapManager->activeMap, data->corners[data->dstCorner])->pos;
        if (fabs(Vector2double_Distance(dst, self->pos)) < 10) {
            if (data->dstCorner + 1 == data->cornerCount) {
                data->dstCorner = 0;
            } else
                data->dstCorner++;
        }

        Vector2double_t heading =  GetHeadingDir(self, &dst);
        Vector2double_Multplydouble(&heading, data->speed);
        self->physics.velocity = heading;

        double angle = 90 + to_degrees(Vector2double_Angle(heading));
        int foward = degrees_to_NSWE(angle);
        switch (foward) {
            case NORTH:
                data->state = MOVEMENT_UP;
                break;
            case EAST:
                data->state = MOVEMENT_RIGHT;
                break;
            case SOUTH:
                data->state = MOVEMENT_DOWN;
                break;
            case WEST:
                data->state = MOVEMENT_LEFT;
                break;
            default:
                break;
        }
    }
    //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%lf",angle);
    //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%d",foward);

}

Vector2double_t GetHeadingDir(RelicEntity_t *self, Vector2double_t *dst) {
    SpiderData *data = ((SpiderData *) self->data);
    Vector2double_t heading = (*dst);
    Vector2double_Substract(&heading, self->pos);
    Vector2double_Normalize(&heading);
    return heading;
}


void DrawSpider(RelicEntity_t *self) {
    SpiderData *data = ((SpiderData *) self->data);
    char action[20];
    switch (data->state) {
        //Walking
        case MOVEMENT_UP:
            strcpy(action, "forward");
            break;
        case MOVEMENT_DOWN:
            strcpy(action, "back");
            break;
        case MOVEMENT_LEFT:
            strcpy(action, "left");
            break;
        case MOVEMENT_RIGHT:
            strcpy(action, "right");
            break;
            //Attacks
        case ATTACK_UP:
            strcpy(action, "attack_forward");
            break;
        case ATTACK_DOWN:
            strcpy(action, "attack_back");
            break;
        case ATTACK_LEFT:
            strcpy(action, "attack_left");
            break;
        case ATTACK_RIGHT:
            strcpy(action, "attack_right");
            break;

        default:
            strcpy(action, "back");
    }

    int id = data->characterData->actions[GetActionSpider(data, action)].strite;
    Sprite_t *sprite = &data->characterData->characterSheat->sprites[id];
    Vector2double_t drawPos = self->pos;
    Vector2double_Substract(&drawPos, (Vector2double_t) {.x=sprite->width / 2, .y=sprite->height / 2});
    RenderSprite_double(g_renderer, sprite, NULL, drawPos,
                        data->state == IDLE,false);

}

void FreeSpider(RelicEntity_t *self) {
    SpiderData *data = self->data;

    free(data->characterData->actions);
    free(data->characterData);

    free(data->corners);
}

RelicEntity_t *CreateSpider(Vector2double_t pos, char *type, char *name, ValueKey *data) {
    struct RelicEntity *res = malloc(sizeof(struct RelicEntity));

    SetupEntity(res, type, name, pos, false);

    SetupFunctions(res, &StartSpider, &UpdateSpider, NULL,NULL, &DrawSpider, &FreeSpider);


    res->data = malloc(sizeof(SpiderData));
    SpiderData *spiderData = res->data;
    spiderData->cooldown = 800;
    spiderData->cooldown_current = 2;

    spiderData->characterData = ParseCharacterFile("spider");
    spiderData->speed = GetDoubleValueKey(spiderData->characterData->propertyes,"speed");
    FreeValuKeyList(spiderData->characterData->propertyes);



    //FIXME: This modifies the original vertexes. Should be a Copy
    Shape *shape = ShapeDuplicate(spiderData->characterData->characterSheat->sprites[0].collision);
    TranslateShape(shape, (Vector2double_t) {.x=-spiderData->characterData->characterSheat->xSize / 2,
            .y=-spiderData->characterData->characterSheat->ySize / 2});

    SetupPhysics(res, false, shape);


    ValueKey *next = data;
    while (next != NULL) {

        if (strcmp(next->key, "corners") == 0) {
            char *buffer = malloc(sizeof(char) * (strlen(next->value) + 1));
            strcpy(buffer, next->value);

            int count = 0;
            for (int i = 0; i < strlen(buffer); ++i) {
                if (buffer[i] == ',')
                    count++;
            }
            count++;
            spiderData->corners = malloc(sizeof(char) * 32 * count);
            spiderData->cornerCount = count;
            spiderData->dstCorner =0;
            spiderData->state = IDLE;

            int i = 0;
            char *pt;
            pt = strtok (next->value,",");
            while (pt != NULL) {
                strcpy(spiderData[0].corners[i],pt);
                pt = strtok (NULL, ",");
                i++;
            }
            free(buffer);
        }

        next = next->next;
    }


    return res;
}

void RegisterSpider() {
    RegisterEntity("enemy_spider"
                   "", &CreateSpider);
}

