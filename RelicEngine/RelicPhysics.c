#include <RelicEngine/RelicMap/RelicMap.h>
#include "RelicPhysics.h"

//Collision

Vector2double_t *GetAxesFromShape(Shape shape) {

    struct Vector2double *axes = malloc(sizeof(struct Vector2double) * shape.vertCount);
    // loop over the vertices
    for (int i = 0; i < shape.vertCount; i++) {
        // get the current vertex
        struct Vector2double p1 = shape.vertices[i];
        // get the next vertex
        struct Vector2double p2 = shape.vertices[i + 1 == shape.vertCount ? 0 : i + 1];
        // subtract the two to get the edge vector
        Vector2double_Substract(&p1, p2);
        struct Vector2double edge = p1;
        // get either perpendicular vector
        struct Vector2double normal = Vector2double_RightNormal(edge);
        Vector2double_Normalize(&normal);
        // the perp method is just (x, y) => (-y, x) or (y, -x)
        axes[i] = normal;
    }
    return axes;
}

Projection ProjectShapeToAxes(Shape shape, Vector2double_t axis) {
    //double min = axis.dot(shape.vertices[0]);
    double min = Vector2double_DotProduct(axis, shape.vertices[0]);
    double max = min;
    for (int i = 1; i < shape.vertCount; i++) {
        // NOTE: the axis must be normalized to get accurate projections
        double p = Vector2double_DotProduct(axis, shape.vertices[i]);
        if (p < min) {
            min = p;
        } else if (p > max) {
            max = p;
        }
    }
    Projection proj = (Projection) {min, max};
    return proj;
}

bool ProjectionOverlap(Projection a, Projection b) {
    return !(a.min > b.max || b.min > a.max);
}

bool ProjectionContainment(Projection a, Projection b) {
    return b.min > a.min && b.max < a.max;
}

double getOverlap(Projection a, Projection b) {
    // make sure they overlap
    if (ProjectionOverlap(a, b)) {
        return fmin(a.max, b.max) - fmax(a.min, b.min);
    }
    return 0;
}

bool Collision(Shape a, Shape b, Vector2double_t *MTV) {

    //The smallest overlap
    double overlap = 10000;// really large value;
    //The axies for the smallest overlap
    Vector2double_t smallest;

    Vector2double_t *axes[2];
    int count[2] = {0};
    axes[0] = GetAxesFromShape(a);
    count[0] = a.vertCount;
    axes[1] = GetAxesFromShape(b);
    count[1] = b.vertCount;

    for (int o = 0; o < 2; ++o) {
        // loop over the axes
        for (int i = 0; i < count[o]; i++) {
            Vector2double_t axis = axes[o][i];
            // project both shapes onto the axis
            Projection p1 = ProjectShapeToAxes(a, axis);
            Projection p2 = ProjectShapeToAxes(b, axis);
            // do the projections overlap?
            if (!ProjectionOverlap(p1, p2)) {
                // then we can guarantee that the shapes do not overlap
                free(axes[0]);
                free(axes[1]);
                return false;
            } else {
                // get the overlap
                double o = getOverlap(p1, p2);
                //See if they are inside each other
                if (ProjectionContainment(p1, p2) || ProjectionContainment(p2, p1)) {
                    // get the overlap plus the distance from the minimum end points
                    double mins = fabs(p1.min - p2.min);
                    double maxs = fabs(p1.max - p2.max);
                    // NOTE: depending on which is smaller you may need to
                    // negate the separating axis!!
                    if (mins < maxs) {
                        o += mins;
                    } else {
                        o += maxs;
                    }
                }
                // check for minimum
                if (o < overlap) {
                    // then set this one as the smallest
                    overlap = o;
                    smallest = axis;
                }
            }
        }
    }

    free(axes[0]);
    free(axes[1]);

    Vector2double_Normalize(&smallest);

    Vector2double_t cacb = ShapeCenter(a);
     Vector2double_Substract(&cacb,ShapeCenter(b)); // ca -> cb
    if (Vector2double_DotProduct(smallest,cacb) < 0) {
        // if the separation vector is in the opposite direction
        // of the center to center vector then flip it around
        // by negating it
        Vector2double_Multplyint(&smallest,-1);
    }

    Vector2double_Multplydouble(&smallest, overlap);
    Vector2double_Add(MTV,smallest);
    //*MTV = smallest;

    return true;

}

bool Collision_physics(Physics *a, Physics *b, Vector2double_t *MTV) {
    //Here we move the 2 Shapes by the position of the entity
    Shape *aShape = ShapeDuplicate(*a->shape);
    TranslateShape(aShape, *a->pos);
    Shape *bShape = ShapeDuplicate(*b->shape);
    TranslateShape(bShape, *b->pos);

    bool res =Collision(*aShape, *bShape, MTV);
    FreeShape(aShape);
    FreeShape(bShape);
    return res;

}

///tilepos in tile coordinates
bool Collision_Tile(Physics *a, Sprite_t tile, Vector2int_t tilePos, Vector2double_t *MTV) {

    Shape *collider = ShapeDuplicate(tile.collision);
    Vector2int_to_WorldCord(g_mapManager->activeMap, &tilePos);
    Vector2double_t worldPos = Vector2int_to_double(tilePos);
    TranslateShape(collider, worldPos);

    Shape *aShape = ShapeDuplicate(*a->shape);
    TranslateShape(aShape, *a->pos);

    bool collision = false;
    if (Collision(*aShape, *collider, MTV)) {
        collision = true;
    }


    FreeShape(collider);
    FreeShape(aShape);

    return collision;

}

bool CollisionCheck(Physics *a, Vector2double_t *MTV) {
    //Run thought on all the objects and check the collision between them and the given collision
    //TODO: Should be simplified to only check the ones that are near
    bool collides = false;

    for (int i = 0; i < g_mapManager->activeMap->entities->count; ++i) {
        RelicEntity_t *other = RelicListGetElement(g_mapManager->activeMap->entities,i);
        if(Vector2double_Distance(*a->pos,other->pos)<224) {
            if (other->hasPhysics && &other->physics != a) {
                //Check if the 2 selected object intersect
                if (Collision_physics(a, &other->physics, MTV)) {
                    collides = true;
                    a->collision = other;
                }
            }
        }
    }

    //check for collision with the tiles near it
    //only the 3x3 area for optimisation.

    //tile pos in the arrays coordinates
    Vector2int_t tilePos = Vector2double_to_int(*a->pos);
    Vector2int_to_tileCord(g_mapManager->activeMap, &tilePos);

    for (int x = tilePos.x - 1; x < tilePos.x + 2; x++) {
        for (int y = tilePos.y - 1; y < tilePos.y + 2; y++) {

            for (int i = 0; i < g_mapManager->activeMap->layerCount; ++i) {
                Sprite_t *sprite = GetMapTileSprite(g_mapManager->activeMap, i, (Vector2int_t) {x, y});

                Vector2int_t worldPos = (Vector2int_t) {x, y};
                Vector2int_to_WorldCord(g_mapManager->activeMap, &worldPos);

                if (sprite != NULL && sprite->hasCollision) {
                    //RenderDebugShapeAtPos(sprite->collision, worldPos, 0xffffffff);

                    if (Collision_Tile(a, *sprite, (Vector2int_t) {x, y}, MTV)) {
                        collides = true;
                    }
                }
            }

            /*RenderDebugRect((Vector2int_t) {worldPos.x + 1, worldPos.y + 1},
                              (Vector2int_t) {worldPos.x + 31, worldPos.y + 31}, 0x0000ffff);
            */
        }
    }

    //RenderDebugPoint((Vector2int_t) {400, 900}, 0x0000ffff);

    return collides;


}

//Physics

Physics *CreatePhysics(Vector2double_t *pos) {
    Physics *res = malloc(sizeof(Physics));
    res->velocity = (Vector2double_t) {0, 0};
    res->pos = pos;
}

void PhysicsUpdate(Physics *obj) {
    obj->collision = NULL;

    Vector2double_t oldPos = *obj->pos;

    //Move the object if the object is not static
    if(!obj->isStatic) {

        obj->pos->x += obj->velocity.x * deltaTime;
        obj->pos->y += obj->velocity.y * deltaTime;

        Vector2double_Substract(&oldPos, *obj->pos);
    }

    Vector2double_t *MTV = malloc(sizeof(Vector2double_t));
    MTV->x = 0;
    MTV->y = 0;

    if (CollisionCheck(obj, MTV)) {
        //Don't move the object if it is static
        if(!obj->isStatic)
        Vector2double_Add(obj->pos, *MTV);
    }

    free(MTV);

}