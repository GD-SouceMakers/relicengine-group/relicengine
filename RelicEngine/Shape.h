//
// Created by dpete on 11/2/2018.
//

#ifndef RELICENGINE_SHAPE_H
#define RELICENGINE_SHAPE_H

#include "Shared.h"
#include "Vector2int.h"

typedef struct Shape {
    int vertCount;
    Vector2double_t *vertices;
} Shape;

Shape *CreateRect(int width,int height);

void TranslateShape(Shape *a, Vector2double_t vec);
Vector2double_t ShapeCenter(Shape s);

Shape *ShapeDuplicate(Shape s);
void FreeShape(Shape *s);

#endif //RELICENGINE_SHAPE_H
