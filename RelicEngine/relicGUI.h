//
// Created by dpete on 11/20/2018.
//

#ifndef RELICENGINE_RELICGUI_H
#define RELICENGINE_RELICGUI_H

void SetupGUI();
void DrawGUI();

#endif //RELICENGINE_RELICGUI_H
