#ifndef RELIC_TEXTURE_MANAGER_H
#define RELIC_TEXTURE_MANAGER_H

#include "Shared.h"
#include "RelicRenderer.h"

struct RelicMap;
struct RelicMapTileSheet;

//#include "SDL.h"

//RelicTexture

//Stores a texture that is loaded into memory
//Mostly tile sheets
typedef struct RelicTexture {
	char name[255];

	SDL_Texture *texture;
	char source[255];

	//only filled out if it is
	int rows;
	int colums;

	int xSize;
	int ySize;

	int startID;
	Sprite_t *sprites;

} RelicTexture_t;

//RelicTextureManager

typedef struct RelicTextureManager {

	RelicTexture_t *textures[255];
	int texturesCount;


} RelicTextureManager_t;

int LoadNeededTextures(struct RelicTextureManager *slef, struct RelicMap *map);
//int LoadSheetTexture(struct RelicTextureManager *self, struct RelicTexture *sheet);
RelicTexture_t *GetTexture(RelicTextureManager_t *self, char *name);

RelicTextureManager_t* Create_TextureManager(void);

void FreeTextureManager(RelicTextureManager_t *self);

struct RelicTextureManager *g_textureManager;

#endif // !RELIC_TEXTURE_MANAGER_H


