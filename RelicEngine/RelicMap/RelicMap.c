#include <RelicEngine/RelicEntitys/RelicCamera.h>
#include <RelicEngine/RelicTextureManager.h>
#include "RelicEngine/RelicEntitys/RelicPlayer.h"
#include "RelicMap.h"

//The "class" that stores a map

int resolveTileIDToSheet(RelicMap_t *self, int GID);

void YSortEntities(RelicMap_t *self);

void DrawMap(RelicMap_t *self) {
    static bool first = true;

    for (int l = 0; l < self->layerCount; l++) {
        if(self->layers[l].visible) {
            Vector2int_t campos = Vector2double_to_int(GetEntityByName(self, "MainCamera")->pos);
            Vector2int_to_tileCord(self, &campos);


            for (int x = campos.x - 15; x < campos.x + 15; x++) {
                for (int y = campos.y - 15; y < campos.y + 15; y++) {

                    int GID = *GetMapTile(self, l, (Vector2int_t) {.x=x, .y=y});
                    if (GID > 0) {

                        int sheet = resolveTileIDToSheet(self, GID);
                        if (sheet > self->sheetsCount) {
                            SDL_LogError(RELICLOG_CATEGORY_MAP_DRAW, "Unknown sheet");
                            continue;
                        }

                        int LID = GID - self->sheets[sheet].startID;

                        Vector2int_t pos = {
                                .x = x * 32, .y = y * 32
                        };

                        RenderSprite(g_renderer, &self->sheets[sheet].texture->sprites[LID], NULL, pos);

                    }
                }
            }

            if (strcmp(self->layers[l].name, "Objects")==0) {
                YSortEntities(self);
                for (int i = 0; i < self->entities->count; ++i) {
                    RelicEntity_t *entity = RelicListGetElement(self->entities, i);
                    if (entity->Draw != NULL)
                        entity->Draw(entity);
                }
            }


            first = false;
        }
    }

    //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"%s",(g_player.pos.x == mainCamera->pos.x) && (g_player.pos.y == mainCamera->pos.y)? "same" : "different");

    //g_player->Draw(g_player);


}

int resolveTileIDToSheet(RelicMap_t *self, int GID) {

    for (int i = 0; i < self->sheetsCount; i++) {
        if (self->sheets[i + 1].startID > GID && self->sheets[i].startID <= GID) {
            return i;
        }
    }

    return -1;
}


int *GetMapTile(RelicMap_t *self, int layer, Vector2int_t pos) {
    return &self->layers[layer].map[(pos.y * self->width) + pos.x];
}

Sprite_t *GetMapTileSprite(RelicMap_t *self, int layer, Vector2int_t pos) {
    if (layer < 0 || layer > self->layerCount ||
        pos.x < 0 || pos.x > self->width ||
        pos.y < 0 || pos.y > self->height) {
        SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Out of the bounds of the map: L:%d , X:%d Y:%d", layer, pos.x, pos.y);
        return NULL;
    }
    int GID = *GetMapTile(self, layer, (Vector2int_t) {.x=pos.x, .y=pos.y});
    if (GID > 0) {

        int sheet = resolveTileIDToSheet(self, GID);
        if (sheet > self->sheetsCount) {
            SDL_LogError(RELICLOG_CATEGORY_MAP_DRAW, "Unknown sheet");
        }

        int LID = GID - self->sheets[sheet].startID;
        return &self->sheets[sheet].texture->sprites[LID];
    }
    return NULL;
}

RelicEntity_t *GetEntityByName(RelicMap_t *self, char *name) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (strcmp(entity->name, name) == 0) {
            return entity;
        }
    }
    return NULL;
}

RelicEntity_t *GetEntityByID(RelicMap_t *self, int id) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (entity->id == id) {
            return entity;
        }
    }
    return NULL;
}

RelicEntity_t *GetEntityByType(RelicMap_t *self, char *type) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (strcmp(entity->type, type) == 0) {
            return entity;
        }
    }
    return NULL;
}

void Swap(void **a, void **b) {
    void *c = *a;
    *a = *b;
    *b = c;
}

int Partition(RelicList *list, int lo, int hi) {
    double pivot = ((RelicEntity_t *) RelicListGetElement(list, hi))->pos.y;
    int i = lo - 1;
    int j = 0;
    for (j = lo; j < hi; ++j) {
        double current = ((RelicEntity_t *) RelicListGetElement(list, j))->pos.y;
        if (current < pivot)
            if (i != j) {
                i = i + 1;
                Swap(RelicListGetElement_p(list, i), RelicListGetElement_p(list, j));
            }
    }


    i = i + 1;
    Swap(RelicListGetElement_p(list, i), RelicListGetElement_p(list, j));
    return i;

}

void QuickSort(RelicList *list, int lo, int hi) {
    if (lo < hi) {
        int p = Partition(list, lo, hi);
        QuickSort(list, lo, p - 1);
        QuickSort(list, p + 1, hi);
    }
}


void YSortEntities(RelicMap_t *self) {
    QuickSort(self->entities, 0, self->entities->count - 1);
}


void Vector2int_to_tileWorldCord(RelicMap_t *self, Vector2int_t *a) {
    a->x = (a->x / self->tileWidth) * self->tileWidth;
    a->y = (a->y / self->tileHeight) * self->tileHeight;
}

void Vector2int_to_tileCord(RelicMap_t *self, Vector2int_t *a) {
    a->x = (a->x / self->tileWidth);
    a->y = (a->y / self->tileHeight);
}

void Vector2int_to_WorldCord(RelicMap_t *self, Vector2int_t *a) {
    a->x = (a->x * self->tileWidth);
    a->y = (a->y * self->tileHeight);
}


void AddEntity(RelicMap_t *self, RelicEntity_t *entity) {
    RelicListAddElement(self->entities, entity);
}

void DestroyEntity(RelicMap_t *self, RelicEntity_t *entity) {

    if (entity->Free != NULL)
        entity->Free(entity);
    if (entity->data != NULL) {
        free (entity->data);
    }
    if (entity->hasPhysics) {
        FreeShape(entity->physics.shape);
    }
    RelicListRemoveElement(self->entities, entity);
    free(entity);
}

void StartAllEntities(RelicMap_t *self) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (entity->Start != NULL) {
            entity->Start(entity);
        }
    }
    SetDefaultLightLevel(self->defaultLight);
}

void UpdateAllEntities(RelicMap_t *self) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (entity->Update != NULL) {
            entity->Update(entity);
        }
    }
}

void LateUpdateAllEntities(RelicMap_t *self) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (entity->LateUpdate != NULL) {
            entity->LateUpdate(entity);
        }
    }
}

void PhysicsUpdateAllEntities(RelicMap_t *self) {
    for (int i = 0; i < self->entities->count; ++i) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, i);
        if (entity->hasPhysics && !(entity->physics.isStatic)) {
            //self->entities[i]->Update(self->entities[i]);
            PhysicsUpdate(&entity->physics);
        }
    }
}


void DestroyLayer(RelicMapLayer_t *self) {
    free(self->map);
    //free(self);
}

void FreeMap(RelicMap_t *self, bool destroyAll) {
    //Free up the layers
    for (int i = 0; i < self->layerCount; ++i) {
        DestroyLayer(&self->layers[i]);
    }
    free(self->layers);
    //Free up the entities
    for (int j = 0; j < self->entities->count; ++j) {
        RelicEntity_t *entity = RelicListGetElement(self->entities, j);
        if (destroyAll || !entity->doNotDestroyOnLoad) {
            if (entity->Free != NULL)
                entity->Free(entity);
            if (entity->data != NULL) {
                free (entity->data);

            }
            if (entity->hasPhysics && entity->physics.shape->vertices != NULL) {

                FreeShape(entity->physics.shape);
            }
            free(entity);
        }
    }
    RelicListFreeList(self->entities);
    //Free up the texture links

    free(self->sheets);

    free(self);

}