#ifndef RELIC_MAP_MANAGER_H
#define RELIC_MAP_MANAGER_H

#include "../Shared.h"

struct RelicMap;

typedef struct RelicMapManager {
	struct RelicMap *activeMap;

	char *nextMapName;
	char *nextMapObject;

} RelicMapManager_t;

RelicMapManager_t* Create_MapManager();
void FreeMapManager();


//struct RelicMap* ParseMap(struct RelicMapManager *self, char *name);
void LoadMap(RelicMapManager_t *self, char *name, char *destName);

void DoMapLoad(RelicMapManager_t *self);

void UnloadMap(RelicMapManager_t *self);

extern RelicMapManager_t *g_mapManager;

#endif // !RELIC_MAP_MANAGER_H

