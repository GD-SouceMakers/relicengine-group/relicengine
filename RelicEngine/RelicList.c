//
// Created by dpete on 11/16/2018.
//

#include "RelicList.h"


RelicList *RelicListCreateList(void) {
    RelicList *res = malloc(sizeof(RelicList));

    res->first = NULL;
    res->last = NULL;
    res->count = 0;

    return res;
}

void RelicListFreeList(RelicList *list) {
    RelicListElement *temp = list->first;
    while (temp != NULL) {
        RelicListElement *next = temp->next;
        free(temp);
        temp = next;
    }
    free(list);
}

void RelicListAddElement(RelicList *self, void *data) {
    RelicListElement *newElement = malloc(sizeof(RelicListElement));
    newElement->next = NULL;
    newElement->data = data;
    if (self->last != NULL) {
        self->last->next = newElement;
        newElement->last = self->last;
        self->last = newElement;
    } else {
        self->last = newElement;
        self->first = newElement;
        newElement->last = NULL;
    }
    self->count++;
}

void RelicListRemoveElement(RelicList *self, void *data) {

    RelicListElement *current = self->first;
    while (current != NULL) {

        if (current->data == data)
            break;

        current = current->next;
    }
    if (current != NULL) {
        current->last->next = current->next;
        if(current->next != NULL) {
            current->next->last = current->last;
        }
        self->count--;
    }
    free(current);
}

void *RelicListGetElement(RelicList *self, int i) {
    int j = 0;
    RelicListElement *current = self->first;
    while (current != NULL) {
        if (j == i) {
            break;
        }
        current = current->next;
        j++;
    }
    if (current != NULL)
        return current->data;
    else
        return NULL;
}

void **RelicListGetElement_p(RelicList *self, int i) {
    int j = 0;
    RelicListElement *current = self->first;
    while (current != NULL) {
        if (j == i) {
            break;
        }
        current = current->next;
        j++;
    }
    if (current != NULL)
        return &current->data;
    else
        return NULL;
}

void *RelicListFindElement(RelicList *self, void *data) {
    RelicListElement *current = self->first;
    while (current != NULL) {

        if (current->data == data)
            break;

        current = current->next;
    }
    if (current != NULL)
        return current->data;
    else
        return NULL;
}
