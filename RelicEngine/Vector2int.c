#include "Vector2int.h"

double to_degrees(double radians) {
    return radians * (180.0 / M_PI);
}

int degrees_to_NSWE(double angle) {
    if (angle < 0)
        angle = 360 + angle;

    if ((angle > 315 && angle < 360 )|| (angle > 0 && angle < 45)) {
        return NORTH;
    }else if (angle > 45 && angle < 135) {
        return EAST;
    }else if (angle > 134 && angle < 225) {
        return SOUTH;
    }else if (angle > 225 && angle < 315) {
        return WEST;
    } else{
        return NORTH;
    }
}

double clamp(double d, double min, double max) {
    const double t = d < min ? min : d;
    return t > max ? max : t;
}

//int

void Vector2int_Add(Vector2int_t *a, Vector2int_t b) {
    a->x += b.x;
    a->y += b.y;
}

void Vector2int_Substract(Vector2int_t *a, Vector2int_t b) {
    a->x -= b.x;
    a->y -= b.y;
}

void Vector2int_Multply(Vector2int_t *a, Vector2int_t b) {
    a->x = a->x * b.x;
    a->y = a->y * b.y;
}

void Vector2int_Multplyint(Vector2int_t *a, int b) {
    a->x = a->x * b;
    a->y = a->y * b;
}

Vector2double_t Vector2int_to_double(Vector2int_t a) {
    Vector2double_t posint = {
            .x = a.x,
            .y = a.y
    };
    return posint;
}

//Double

void Vector2double_Add(Vector2double_t *a, Vector2double_t b) {
    a->x += b.x;
    a->y += b.y;
}

void Vector2double_Substract(Vector2double_t *a, Vector2double_t b) {
    a->x -= b.x;
    a->y -= b.y;
}

void Vector2double_Multply(Vector2double_t *a, Vector2double_t b) {
    a->x = a->x * b.x;
    a->y = a->y * b.y;
}

void Vector2double_Multplyint(Vector2double_t *a, int b) {
    a->x = a->x * b;
    a->y = a->y * b;
}

void Vector2double_Multplydouble(Vector2double_t *a, double b) {
    a->x = a->x * b;
    a->y = a->y * b;
}

double Vector2double_Magnitude(Vector2double_t a) {
    return sqrt(pow(a.x, 2) + pow(a.y, 2));
}

void Vector2double_Normalize(Vector2double_t *a) {
    double mag = Vector2double_Magnitude(*a);
    a->x = a->x / mag;
    a->y = a->y / mag;
}

Vector2double_t Vector2double_LeftNormal(Vector2double_t a) {
    return (Vector2double_t) {.x=-a.y, .y=a.x};
}

Vector2double_t Vector2double_RightNormal(Vector2double_t a) {
    return (Vector2double_t) {.x=a.y, .y=-a.x};
}

double Vector2double_DotProduct(Vector2double_t a, Vector2double_t b) {
    return a.x * b.x + a.y * b.y;
}

Vector2double_t Vector2double_Lerp(Vector2double_t a, Vector2double_t b, int i) {
    Vector2double_t res;

    res.x = a.x - a.x / 100 * i;
    res.y = a.y - a.y / 100 * i;

    return res;
}

double Vector2double_Distance(Vector2double_t a, Vector2double_t b) {
    Vector2double_Substract(&a, b);
    return Vector2double_Magnitude(a);
}

double Vector2double_Angle(Vector2double_t a) {
    return atan2(a.y , a.x);
}


Vector2int_t Vector2double_to_int(Vector2double_t a) {
    Vector2int_t posint = {
            .x = floor(a.x),
            .y = floor(a.y)
    };
    return posint;
}
