#include "RelicRenderer.h"
#include "RelicTimer.h"
#include "RelicTextureManager.h"

#include "Vector2int.h"
#include "RelicEngine/RelicEntitys/RelicPlayer.h"
#include "RelicEntitys/RelicCamera.h"

struct RelicRenderer;
//struct Sprite_t;
struct Shader_t;

void DrawPlayerDebug(struct RelicMap *activeMap);

void FinishLight();

TTF_Font *debugFont;
TTF_Font *debugFont_outline;
int outline = 2;

SDL_Color withe = {255, 255, 255}, black = {0, 0, 0};

int bkgLight;
int overlayLight;


SDL_BlendMode lightDrawBelnd;

//Render Sprites
void
RenderSprite_anim(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2int_t pos, bool noAnim, bool stopLastFrame) {

    SDL_Rect sprite = {.x=i->x, .y=i->y, .w= i->width, .h= i->height};
    SDL_Rect dest = {.x= pos.x, .y= pos.y, .w=i->width, .h=i->height};

    if (i->frameCount > 0 && !noAnim) {

        if (i->frames[i->currentFrame].time <= timeSinceStart - i->lastTime) {
            if (!stopLastFrame && i->currentFrame + 1 >= i->frameCount) {
                i->currentFrame = 0;
            } else if (i->currentFrame + 1 < i->frameCount) {
                i->currentFrame++;
            }

            i->lastTime = timeSinceStart;
        }
        Sprite_t *a = i->frames[i->currentFrame].sprite;
        sprite = (SDL_Rect) {a->x, a->y, a->width, a->height};
    } else if (i->frameCount > 0) {
        Sprite_t *a = i->frames[i->currentFrame].sprite;
        sprite = (SDL_Rect) {a->x, a->y, a->width, a->height};
    }

    SDL_RenderCopy(self->sdlRenderer, i->texture->texture, &sprite, &dest);
}

void RenderSprite(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2int_t pos) {
    WorldToScreen(mainCamera, &pos);
    RenderSprite_anim(self, i, s, pos, false, false);
}

void RenderSprite_double(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2double_t pos, bool noAnim,
                         bool stopLastFrame) {
    Vector2int_t posint = Vector2double_to_int(pos);
    WorldToScreen(mainCamera, &posint);

    RenderSprite_anim(self, i, s, posint, noAnim, stopLastFrame);
}

void
RenderSprite_lighting(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2int_t pos, SDL_Color color, bool noAnim) {
    WorldToScreen(mainCamera, &pos);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->lightingMap);

    SDL_SetTextureBlendMode(i->texture->texture, lightDrawBelnd);
    SDL_SetTextureColorMod(i->texture->texture, color.r, color.g, color.b);
    //SDL_SetTextureAlphaMod(i->texture->texture,255);

    RenderSprite_anim(self, i, s, pos, noAnim, false);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void RenderSprite_lighting_double(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2double_t pos, SDL_Color color,
                                  bool noAnim) {
    Vector2int_t posint = Vector2double_to_int(pos);

    RenderSprite_lighting(self, i, s, posint, color, noAnim);
}

//SetRenderParams
void SetDefaultLightLevel(int lightLevel) {
    bkgLight = lightLevel;
}

void SetOverlayLightLevel(int lightLevel) {
    overlayLight = lightLevel;
}


//Debug Renderers
void RenderDebugRect(Vector2int_t a, Vector2int_t b, Uint32 color) {
    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->debugTarget);
    WorldToScreen(mainCamera, &a);
    WorldToScreen(mainCamera, &b);

    rectangleColor(g_renderer->sdlRenderer, a.x, a.y, b.x, b.y, color);
    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void RenderDebugPoint(Vector2int_t a, Uint32 color) {

    WorldToScreen(mainCamera, &a);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->debugTarget);
    filledCircleColor(g_renderer->sdlRenderer, a.x, a.y, 5, color);
    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void RenderDebugShapeAtPos(Shape shape, Vector2int_t pos, Uint32 color) {
    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->debugTarget);

    Sint16 *vx = malloc(
            sizeof(int) * shape.vertCount);
    Sint16 *vy = malloc(
            sizeof(int) * shape.vertCount);
    for (int j = 0; j < shape.vertCount; ++j) {
        Vector2int_t renderPos = Vector2double_to_int(shape.vertices[j]);
        Vector2int_Add(&renderPos, pos);
        WorldToScreen(mainCamera, &renderPos);
        vx[j] = renderPos.x;
        vy[j] = renderPos.y;
    }

    filledPolygonColor(g_renderer->sdlRenderer, vx, vy, shape.vertCount, color);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);

    free(vx);
    free(vy);
}

SDL_Surface *DrawTextToSurface(TTF_Font *font, char *text, SDL_Color color, Vector2int_t pos) {
    SDL_Surface *text_surface;

    SDL_Rect dest;

    text_surface = TTF_RenderUTF8_Blended(font, text, color);

    if (!(text_surface)) {
        //handle error here, perhaps print TTF_GetError at least
        SDL_LogError(RELICLOG_CATEGORY_ERROR, "TTF_ERROR: %s\n", TTF_GetError());
    }

    return text_surface;

}

void RenderDebugText(Vector2int_t pos, char *text) {

    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->debugTarget);

    SDL_Texture *texture;

    SDL_Rect dest = {0, 0, 0, 0};

    SDL_Surface *outline_surface = DrawTextToSurface(debugFont_outline, text, withe, pos);
    SDL_Surface *text_surface = DrawTextToSurface(debugFont, text, black, pos);


    SDL_Rect rect = {outline, outline, outline_surface->w, outline_surface->h};

    /* blit text onto its outline */
    SDL_SetSurfaceBlendMode(text_surface, SDL_BLENDMODE_BLEND);
    SDL_BlitSurface(text_surface, NULL, outline_surface, &rect);


    texture = SDL_CreateTextureFromSurface(g_renderer->sdlRenderer, outline_surface);
    dest.x = pos.x;
    dest.y = pos.y;
    dest.w = outline_surface->w;
    dest.h = outline_surface->h;
    SDL_RenderCopy(g_renderer->sdlRenderer, texture, NULL, &dest);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);

    SDL_FreeSurface(outline_surface);
    SDL_FreeSurface(text_surface);

    SDL_DestroyTexture(texture);
}

//GUI
void RenderGUIText(SDL_Texture *target, Vector2int_t pos, TTF_Font *font, char *text, SDL_Color color) {

    if (target == NULL)
        target = g_renderer->guiTarget;
    SDL_SetRenderTarget(g_renderer->sdlRenderer, target);

    SDL_Texture *texture;

    SDL_Surface *text_surface = DrawTextToSurface(font, text, color, pos);

    /* blit text onto its outline */
    SDL_SetSurfaceBlendMode(text_surface, SDL_BLENDMODE_BLEND);

    texture = SDL_CreateTextureFromSurface(g_renderer->sdlRenderer, text_surface);
    SDL_Rect dest = {0, 0, 0, 0};
    dest.x = pos.x;
    dest.y = pos.y;
    dest.w = text_surface->w;
    dest.h = text_surface->h;
    SDL_RenderCopy(g_renderer->sdlRenderer, texture, NULL, &dest);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);

    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(texture);
}

SDL_Rect RenderGuiTextCentered(SDL_Texture *target, Vector2int_t pos, TTF_Font *font, char *text, SDL_Color color) {
    if (target == NULL)
        target = g_renderer->guiTarget;
    SDL_SetRenderTarget(g_renderer->sdlRenderer, target);

    SDL_Texture *texture;

    SDL_Surface *text_surface = DrawTextToSurface(font, text, color, pos);

    /* blit text onto its outline */
    SDL_SetSurfaceBlendMode(text_surface, SDL_BLENDMODE_BLEND);

    texture = SDL_CreateTextureFromSurface(g_renderer->sdlRenderer, text_surface);
    SDL_Rect dest = {0, 0, 0, 0};
    dest.x = pos.x - text_surface->w / 2;
    dest.y = pos.y - text_surface->h / 2;
    dest.w = text_surface->w;
    dest.h = text_surface->h;
    SDL_RenderCopy(g_renderer->sdlRenderer, texture, NULL, &dest);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);

    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(texture);

    return dest;
}

SDL_Rect GetRenderGUITextSize(TTF_Font *font, char *text) {
    SDL_Surface *text_surface = DrawTextToSurface(font, text, withe, (Vector2int_t) {0, 0});
    SDL_Rect dest = {0, 0, 0, 0};
    dest.x = 0;
    dest.y = 0;
    dest.w = text_surface->w;
    dest.h = text_surface->h;
    return dest;


}

void RenderGUISprite(SDL_Texture *target, Sprite_t *i, Shader_t *s, Vector2int_t pos, SDL_Color color, bool noAnim) {


    if (target == NULL)
        target = g_renderer->guiTarget;
    SDL_SetRenderTarget(g_renderer->sdlRenderer, target);


    SDL_SetTextureColorMod(i->texture->texture, color.r, color.g, color.b);
    //SDL_SetTextureAlphaMod(i->texture->texture,255);

    RenderSprite_anim(g_renderer, i, s, pos, noAnim, false);

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void RenderGUIRect(SDL_Texture *target, Vector2int_t a, int width, int height, SDL_Color color) {

    if (target == NULL)
        target = g_renderer->guiTarget;
    SDL_SetRenderTarget(g_renderer->sdlRenderer, target);

    SDL_SetRenderDrawColor(g_renderer->sdlRenderer, color.r, color.g, color.b, color.a);
    SDL_Rect rect = (SDL_Rect) {.x=a.x, .y=a.y, .w=width, .h=height};
    SDL_RenderFillRect(g_renderer->sdlRenderer, &rect);
    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

//Final Draw

void RenderTextureToTarget(SDL_Texture *texture, SDL_Texture *target) {
    SDL_SetRenderTarget(g_renderer->sdlRenderer, target);
    SDL_RenderCopyEx(g_renderer->sdlRenderer, texture, NULL, NULL, 0, NULL, SDL_FLIP_NONE);
}

void ClearTexture(SDL_Texture *texture, SDL_Color color) {
    SDL_SetRenderTarget(g_renderer->sdlRenderer, texture);
    SDL_SetRenderDrawColor(g_renderer->sdlRenderer, color.r, color.g, color.b, color.a);
    SDL_RenderClear(g_renderer->sdlRenderer);
    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void DrawPlayerDebug(struct RelicMap *activeMap) {
    RelicEntity_t *player = GetEntityByType(activeMap, "player");

    Vector2int_t player_debug = Vector2double_to_int(player->pos);
    Vector2int_to_tileWorldCord(activeMap, &player_debug);

    Vector2int_t player_debug2 = {.x = player_debug.x + 32, .y=player_debug.y + 32};

    RenderDebugRect(player_debug, player_debug2, 0xff0000ff);
    //RenderDebugPoint(player_debug, 0xff9000ff);
    RenderDebugPoint(Vector2double_to_int(player->pos), 0xff9090ff);
}

void DebugDraw() {

    //Draw the grid
    struct RelicMap *activeMap = g_mapManager->activeMap;

    if (g_mapManager != NULL && activeMap != NULL) {

        Vector2int_t campos = Vector2double_to_int(GetEntityByName(activeMap, "MainCamera")->pos);
        Vector2int_to_tileCord(activeMap, &campos);

        RelicEntity_t *player = GetEntityByType(activeMap, "player");

        for (int x = campos.x - 5; x < campos.x + 5; x++) {
            for (int y = campos.y - 5; y < campos.y + 5; y++) {

                Vector2int_t pos1 = {.x = x * 32, .y=y * 32};

                Vector2int_t pos2 = {.x = (x + 1) * 32, .y=(y + 1) * 32};

                //RenderDebugRect(pos1, pos2, 0xffffffff);


                for (int i = 0; i < activeMap->layerCount; ++i) {
                    Sprite_t *sprite = GetMapTileSprite(activeMap, 0, (Vector2int_t) {.x=x, .y=y});
                    if (sprite != NULL && sprite->hasCollision) {
                        Shape shape = sprite->collision;
                        //RenderDebugShapeAtPos(shape, pos1, 0x00ff9030);
                    }
                }
            }
        }
        for (int j = 0; j < activeMap->entities->count; ++j) {
            RelicEntity_t *pEntity = RelicListGetElement(activeMap->entities, j);

            if(Vector2double_Distance(pEntity->pos,player->pos)<32) {
                if (pEntity->hasPhysics) {
                    Vector2double_t pos = pEntity->pos;
                    //RenderDebugShapeAtPos(*pEntity->physics.shape, Vector2double_to_int(pos), 0xffF090);
                    RenderDebugPoint(Vector2double_to_int(pEntity->pos), 0xff9090ff);
                }
            }
            if(strcmp(pEntity->type,"collision")!=0) {
                char text[32];
                sprintf(text, "[%d]:%s:%s", pEntity->id, pEntity->type, pEntity->name);
                RenderDebugText((Vector2int_t) {0, g_renderer->height - 15 - j * 10}, text);
            }
        }

        DrawPlayerDebug(activeMap);

        for (int k = 0; k < activeMap->sheetsCount; ++k) {
            char text[32];
            sprintf(text, "[%d]:%s", k, activeMap->sheets[k].source);
            RenderDebugText((Vector2int_t) {420, g_renderer->height - 15 - k * 10}, text);
        }


    }
    if (g_textureManager != NULL) {
        for (int i = 0; i < g_textureManager->texturesCount; ++i) {
            char text[32];
            sprintf(text, "[%d]:%s", i, g_textureManager->textures[i]->source);
            RenderDebugText((Vector2int_t) {230, g_renderer->height - 15 - i * 10}, text);
        }
    }


    char text[32];
    sprintf(text, "[FPS]:%lf", 1 / deltaTime);
    RenderDebugText((Vector2int_t) {0, 0}, text);


    SDL_SetRenderTarget(g_renderer->sdlRenderer,NULL);
}

void FinishLight() {
    uint8_t light = 255 - bkgLight;

    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->lightingMap);
    SDL_SetRenderDrawColor(g_renderer->sdlRenderer, light, light, light, light);
    SDL_SetRenderDrawBlendMode(g_renderer->sdlRenderer, SDL_BLENDMODE_ADD);
    SDL_RenderFillRect(g_renderer->sdlRenderer, &(SDL_Rect) {.x=0, .y=0, .w=g_renderer->width, .h=g_renderer->height});

    light = 255 - overlayLight;

    SDL_SetRenderTarget(g_renderer->sdlRenderer, g_renderer->overlayTarget);
    SDL_SetRenderDrawColor(g_renderer->sdlRenderer, light, light, light, light);
    SDL_SetRenderDrawBlendMode(g_renderer->sdlRenderer, SDL_BLENDMODE_NONE);
    SDL_RenderFillRect(g_renderer->sdlRenderer, &(SDL_Rect) {.x=0, .y=0, .w=g_renderer->width, .h=g_renderer->height});

    SDL_SetRenderTarget(g_renderer->sdlRenderer, NULL);
}

void Present(RelicRenderer *self) {
    DebugDraw();

    //Finish the lighthing
    FinishLight();

    SDL_SetTextureBlendMode(g_renderer->lightingMap, SDL_BLENDMODE_MOD);
    SDL_RenderCopyEx(g_renderer->sdlRenderer, g_renderer->lightingMap, NULL, NULL, 0, NULL, SDL_FLIP_NONE);

    SDL_RenderCopyEx(g_renderer->sdlRenderer, g_renderer->overlayTarget, NULL, NULL, 0, NULL, SDL_FLIP_NONE);

    SDL_RenderCopyEx(g_renderer->sdlRenderer, g_renderer->guiTarget, NULL, NULL, 0, NULL, SDL_FLIP_NONE);

#if DEBUG
    SDL_RenderCopyEx(g_renderer->sdlRenderer, g_renderer->debugTarget, NULL, NULL, 0, NULL, SDL_FLIP_NONE);
#endif
    SDL_RenderPresent(self->sdlRenderer);
}


void Clear(RelicRenderer *self) {

    //Clear debug
    SDL_SetRenderTarget(self->sdlRenderer, self->debugTarget);
    SDL_SetRenderDrawColor(self->sdlRenderer, 0, 0, 0, 0);
    SDL_RenderClear(self->sdlRenderer);
    SDL_SetRenderTarget(self->sdlRenderer, NULL);

    //Clear light
    SDL_SetRenderTarget(self->sdlRenderer, self->lightingMap);
    SDL_SetRenderDrawColor(self->sdlRenderer, 0, 0, 0, 255);
    SDL_RenderClear(self->sdlRenderer);
    SDL_SetRenderTarget(self->sdlRenderer, NULL);

    //Clear light
    SDL_SetRenderTarget(self->sdlRenderer, self->overlayTarget);
    SDL_SetRenderDrawColor(self->sdlRenderer, 0, 0, 0, 255);
    SDL_RenderClear(self->sdlRenderer);
    SDL_SetRenderTarget(self->sdlRenderer, NULL);

    //Clear GUI
    SDL_SetRenderTarget(self->sdlRenderer, self->guiTarget);
    SDL_SetRenderDrawColor(self->sdlRenderer, 0, 0, 0, 0);
    SDL_RenderClear(self->sdlRenderer);
    SDL_SetRenderTarget(self->sdlRenderer, NULL);

    SDL_SetRenderDrawColor(self->sdlRenderer, 0, 0, 0, 255);
    SDL_RenderClear(self->sdlRenderer);
}

//Create the game window
void createWindow(RelicRenderer *renderer) {

    SDL_Window *window = SDL_CreateWindow("RELIC ENGINE", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          renderer->width,
                                          renderer->height, 0);
    if (window == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_ERROR, "Can't create window: %s", SDL_GetError());
        exit(1);
    }

    renderer->window = window;
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Window size: w:%d h:%d", w, h);
}

void createDebugImage(RelicRenderer *renderer) {
    //Make a target texture to render too
    renderer->debugTarget = SDL_CreateTexture(renderer->sdlRenderer, SDL_PIXELFORMAT_RGBA8888,
                                              SDL_TEXTUREACCESS_TARGET, renderer->width, renderer->height);

    SDL_SetTextureBlendMode(renderer->debugTarget, SDL_BLENDMODE_BLEND);
}

void createGUIImage(RelicRenderer *renderer) {
    //Make a target texture to render too
    renderer->guiTarget = SDL_CreateTexture(renderer->sdlRenderer, SDL_PIXELFORMAT_RGBA8888,
                                            SDL_TEXTUREACCESS_TARGET, renderer->width, renderer->height);

    SDL_SetTextureBlendMode(renderer->guiTarget, SDL_BLENDMODE_BLEND);
}

void createLightLayer(RelicRenderer *renderer) {

    //Make a target texture to render too
    renderer->lightingMap = SDL_CreateTexture(renderer->sdlRenderer, SDL_PIXELFORMAT_RGBA8888,
                                              SDL_TEXTUREACCESS_TARGET, renderer->width, renderer->height);

    SDL_SetTextureBlendMode(renderer->lightingMap, SDL_BLENDMODE_MOD);

    renderer->overlayTarget = SDL_CreateTexture(renderer->sdlRenderer, SDL_PIXELFORMAT_RGBA8888,
                                                SDL_TEXTUREACCESS_TARGET, renderer->width, renderer->height);

    SDL_SetTextureBlendMode(renderer->overlayTarget, SDL_BLENDMODE_MOD);

    lightDrawBelnd = SDL_ComposeCustomBlendMode(
            SDL_BLENDFACTOR_SRC_ALPHA,
            SDL_BLENDFACTOR_ONE,
            SDL_BLENDOPERATION_ADD,
            SDL_BLENDFACTOR_ZERO,
            SDL_BLENDFACTOR_ZERO,
            SDL_BLENDOPERATION_ADD
    );

}

void InitTTFSystem() {
    if (TTF_Init() == -1) {
        SDL_LogError(RELICLOG_CATEGORY_ERROR, "TTF_Init: %s\n", TTF_GetError());
        exit(2);
    }

    //Debug font load
    debugFont = TTF_OpenFont("consola.ttf", 10);
    debugFont_outline = TTF_OpenFont("consola.ttf", 10);
    TTF_SetFontOutline(debugFont_outline, outline);
    if (!debugFont) {
        SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Can't load the font: %s\n", TTF_GetError());
    }
}

RelicRenderer *RelicCreateRenderer() {
    if (g_renderer != NULL) {
        SDL_Log("Can't init the renderer, a instance already exists");
    }

    RelicRenderer *ren = malloc(sizeof(RelicRenderer));
    ren->height = 480;
    ren->width = 640;

    createWindow(ren);


    //The text system
    InitTTFSystem();

    ren->sdlRenderer = SDL_CreateRenderer(ren->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE |
                                                           SDL_RENDERER_PRESENTVSYNC);
    if (ren->sdlRenderer == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_ERROR, "Can't init the renderer: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(ren->sdlRenderer);

    createDebugImage(ren);
    createLightLayer(ren);
    createGUIImage(ren);

    g_renderer = ren;

    return ren;
}

void FreeRenderer() {

    free(g_renderer);
}

