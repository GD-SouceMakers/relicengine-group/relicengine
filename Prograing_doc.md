# Relic Engine Programing Manual

## Startup
The ``program.c`` file creates a instance of ``RelicApp`` and calls ``InitApp`` witch sets up the systems for the game.
It sets the global variables for systems and initialises them.

## Start
The ``program.c`` file calls ``StartApp`` with the inited ``RelicApp`` struct.
This is the call that Starts the actual game play loop. 
### The game play loop
First we get all the events that has happened since the last loop and process them using the ``HandleInput`` function. 
It puts the input it go into a global struct that stores game specific input data.
Than we check what state the game is in
- ``END_GAME``
    tells us that the player has died and we need to clear the map.
- ``NEW_MAP``
This state tells us that there is a map that needs to be loaded in
- Else
In this case we check if we have a loaded map and if so we run the updates
  - Update all the entities. ``UpdateAllEntities``
  - Do the physics update. ``PhysicsUpdateAllEntities``
  - Do post physics update called Late Update. ``LateUpdateAllEntities``
  - Draw the actual Map ``DrawMap``
  
- Draw the gui
- Present everything that has been drawn
- Clear the render Targets.
- Calculate the time this update took


## File Loading
File loading is handled by ``RelicFileLoad.c``

This has functions to load in the File form the game Folder (``rcf,rmf,rsf``)
These files have been designed to be easy to read, the first part of them are strictly ordered to allow for easy read
The most important functions in this file are the Parsing functions
- ``ParseMapFile`` 
This allocates space for and loads a given map file form the ``GAME/Maps`` folder (only the actual name should be passed in without extension)
- ``ParseSheetFile`` 
This allocates space for and loads a given sheet file form the ``GAME/Textures`` folder (only the actual name should be passed in without extension)
- ``ParseCharacterFile`` 
This allocates space for and loads a given character file form the ``GAME`` folder (only the actual name should be passed in without extension)

## Managgers
### Texture Manager
This manages and when needed frees the textures. You should always load a texture with it's ``GetTexture`` function

### Map Manager
This is responsible for loading and freeing maps.
To request a maps load ``LoadMap`` This will be loaded at the start of the next update loop
To unload a map use ``UnloadMap``

## Renderer
This houses the functions that are responsible for rendering to the screen and combining the render targets at the Present stage
- The GUI draw functions all draw to the GUI render target
- The Light draws draw to the Light draw target
- The Debug draws draw to the Debug draw target

### Lighting
There are 2 ways
- Light is the layer that receives the light "circles". You can set the light level with ``SetDefaultLightLevel``
- Overlay layer is for fading the hole screen, you can't draw to it. To set the level use ``SetOverlayLightLevel``

### Final Draw 
This part is responsible for doing the debug draw and combing all the render targets.
Combine order:
Light -> Overlay -> Gui -> DEBUG
Than present it.

## Map
This is a struct that hold all the data for a map
Entities, tile data.

This handles the drawing of the map as well:
It draws the layers in oder and all the entities if it finds a layer called ``Objects``
The entities are sorted with a quick sort to be by their y position.

## Entity system
The entities are ``RelicEntity`` structs with the proper Callback functions assigned at creation
### Creation
To create a new entity you need to call the ``CreateEntityOfType`` witch creates a new struct for you,
form the types that has been registered into the ``RelicRegisty``.
### Registration 
To register a new entity you need to call ``RegisterEntity`` with the name and creator function of the entity
### Drawing
To draw a entity, it has to have set it's draw call to point to a function. The draw function can use any method to draw to the screen.
The suggested mode is to use the engines ``RenderSprite_anim`` or ``RenderSprite`` functions and the respective light render calls.
It is important to reset the render target if it was changed during the call.
### Update
To make any changes to the state of the entity you should use the ``Update`` callback.
In this callback you can act on the inputs form the user using the ``globalInput`` global variable. 
### Free
If you have any special memory allocations in the entity you should use this callback to free it up.

## Physics
The physics update is performed once every update.
It has collision detection and uses SAT (Separating Axes Theorem) for it.
It stores one collision data in the physics struct of the Entity

It only exposes one function: ``PhysicsUpdate`` witch runs the update on the Given Physics object
It moves it by it's velocity and check for collisions (witch get resolved by moving the current entity)

Phyisics uses ``Shape`` to store collision data. 
``Shape`` has a few utility functions witch help in the creation and operations on the struct.

## GUI
The GUI is completely state driven. It only has a single call that draws the gui according to it's stae
It uses a state machine to draw the 3 different states, Main menu/In game/End
it has a special function that handles these states using function pointers. 

## RelicList
It is a linked list implementation, that uses ``void*`` as data to be able to store any kind of data