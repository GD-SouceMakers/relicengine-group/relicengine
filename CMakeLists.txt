cmake_minimum_required(VERSION 3.12)
project(RelicEngine)

set(CMAKE_CXX_STANDARD 14)

set(_CRT_SECURE_NO_WARNINGS 1)

include(lib/cmake/sdl2-config.cmake)

include_directories(lib/SDL2/include)
include_directories(.)
include_directories(RelicEngine)
include_directories(RelicEngine/RelicMap)


add_executable(RelicEngine
        ${SDL2_EXEC}
        RelicEngine/RelicMap/RelicMap.c RelicEngine/RelicMap/RelicMap.h
        RelicEngine/RelicFileLoad.c RelicEngine/RelicFileLoad.h
        RelicEngine/RelicMap/RelicMapManager.c RelicEngine/RelicMap/RelicMapManager.h
        RelicEngine/RelicApp.c RelicEngine/RelicApp.h
        RelicEngine/RelicRenderer.c RelicEngine/RelicRenderer.h
        RelicEngine/RelicTextureManager.c RelicEngine/RelicTextureManager.h
        RelicEngine/RelicEntitys/RelicPlayer.h RelicEngine/RelicEntitys/RelicPlayer.c
        RelicEngine/RelicTimer.h
        RelicEngine/Shared.h
        RelicEngine/Singletons.h
        RelicEngine/Vector2int.h RelicEngine/Vector2int.c
        Program.c Program.h
        RelicEngine/RelicPhysics.c RelicEngine/RelicPhysics.h
        RelicEngine/Shape.c RelicEngine/Shape.h
        RelicEngine/lib/debugmalloc.c RelicEngine/lib/debugmalloc.h
        RelicEngine/RelicEntitys/RelicCamera.c RelicEngine/RelicEntitys/RelicCamera.h
        RelicEngine/RelicEntitys/RelicEntity.c RelicEngine/RelicEntitys/RelicEntity.h
        RelicEngine/RelicEntitys/RelicRegisty.c RelicEngine/RelicEntitys/RelicRegisty.h
        RelicEngine/RelicEntitys/EmptyEntity.c RelicEngine/RelicEntitys/EmptyEntity.h
        RelicEngine/RelicEntitys/MapChange.c RelicEngine/RelicEntitys/MapChange.h
        RelicEngine/RelicEntitys/LightSource.c RelicEngine/RelicEntitys/LightSource.h
        RelicEngine/RelicList.c RelicEngine/RelicList.h
        RelicEngine/RelicEntitys/Spider.c RelicEngine/RelicEntitys/Spider.h
        RelicEngine/RelicGUI.c RelicEngine/RelicGUI.h
        RelicEngine/RelicEntitys/BasicCollider.c RelicEngine/RelicEntitys/BasicCollider.h)

set(SDL_LIB P:/c/relicengine/lib/SDL2/lib)
target_link_libraries(RelicEngine mingw32)

target_link_libraries(RelicEngine ${SDL2_TARGETS})

target_link_libraries(RelicEngine user32)
target_link_libraries(RelicEngine gdi32)
target_link_libraries(RelicEngine winmm)
target_link_libraries(RelicEngine dxguid)

if (DEBUG_LEVEL)
    add_definitions(-DDEBUG_LEVEL=${DEBUG_LEVEL})
    add_definitions(-DDEBUG=true)
else()
    add_definitions(-DDEBUG_LEVEL=-1)
    add_definitions(-DDEBUG=false)
endif()

set_property(TARGET RelicEngine PROPERTY C_STANDARD 11)