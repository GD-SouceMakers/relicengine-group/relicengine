<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="TileSet_002" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="TileSet_002.png" trans="4b637f" width="512" height="512"/>
 <tile id="206">
  <animation>
   <frame tileid="206" duration="180"/>
   <frame tileid="232" duration="180"/>
   <frame tileid="234" duration="180"/>
   <frame tileid="236" duration="180"/>
   <frame tileid="238" duration="180"/>
  </animation>
 </tile>
 <tile id="207">
  <animation>
   <frame tileid="207" duration="180"/>
   <frame tileid="233" duration="180"/>
   <frame tileid="235" duration="180"/>
   <frame tileid="237" duration="180"/>
   <frame tileid="239" duration="180"/>
  </animation>
 </tile>
 <tile id="222">
  <animation>
   <frame tileid="222" duration="180"/>
   <frame tileid="248" duration="180"/>
   <frame tileid="250" duration="180"/>
   <frame tileid="252" duration="180"/>
   <frame tileid="254" duration="180"/>
  </animation>
 </tile>
 <tile id="223">
  <animation>
   <frame tileid="223" duration="180"/>
   <frame tileid="249" duration="180"/>
   <frame tileid="251" duration="180"/>
   <frame tileid="253" duration="180"/>
   <frame tileid="255" duration="180"/>
  </animation>
 </tile>
 <tile id="240">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="2.625" y="-0.125" width="29.125" height="48"/>
  </objectgroup>
 </tile>
 <tile id="241">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0" y="0" width="32" height="48"/>
  </objectgroup>
 </tile>
 <tile id="242">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0" y="0" width="29" height="48"/>
  </objectgroup>
 </tile>
 <tile id="243">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="3" y="0" width="29" height="48"/>
  </objectgroup>
 </tile>
 <tile id="244">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0" y="0" width="26" height="48"/>
  </objectgroup>
 </tile>
 <tile id="245">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="3" y="0" width="29" height="48"/>
  </objectgroup>
 </tile>
 <tile id="246">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0" y="0" width="29" height="48"/>
  </objectgroup>
 </tile>
</tileset>
