<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="gui_menu_01" tilewidth="64" tileheight="64" tilecount="10" columns="5">
 <image source="gui_menu_01.png" trans="4b637f" width="320" height="160"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="180"/>
   <frame tileid="1" duration="180"/>
   <frame tileid="2" duration="180"/>
   <frame tileid="3" duration="180"/>
   <frame tileid="4" duration="180"/>
  </animation>
 </tile>
</tileset>
